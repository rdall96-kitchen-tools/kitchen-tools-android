# Kitchen-Tools
A kitchen toolset for Android.

## Open Beta Available on the Google Play Store
Current version: v1.0.16 - [Join the Kitchen Tools Beta](https://play.google.com/apps/testing/com.rdall96.kitchentools)

## CS 490 Project (Kotlin)
### Android Development

The idea for the project consists in the development of a small application for Android using Kotlin and standard Android SDK (API 26) tools. This application revolves around the idea to create a toolset for the kitchen, featuring unit converters, timers and lists, allowing users to keep track of everything they need all in one place.

Here's a run-down of what each component shall do:

- CONVERTER: The converters will contain units like volume and weight, to allow converting between different measuring systems or quantities.

- TIMER: The timers will allow to keep track of different items during the process of cooking independently from each other. This would make the cooks life easier by having a list of things ordered by what will finish first, for example.

- LIST: The schedulers are currently still in the thought process. They could be some way to write down a list of things to do, by assigning an estimate preparation, cook time, and ingredients list.
