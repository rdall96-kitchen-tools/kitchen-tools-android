package com.rdall96.kitchentools

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView

/*
 * IconSpinnerAdapter.kt
 * Project: Kitchen-Tools
 *
 * Author: Ricky Dall'Armellina
 * 4/7/19
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 * 
 */

class IconSpinnerAdapter(context: Context, private val dataSource: Array<String>, private val iconName: String): BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        // Get view for row item
        val view: View
        val holder: ViewHolder

        if(convertView == null) {
            view = inflater.inflate(R.layout.icon_spinner_image_item, parent, false)
            holder = ViewHolder()
            holder.iconName = view.findViewById(R.id.timer_icon_grid_name)
            holder.iconImage = view.findViewById(R.id.timer_icon_grid_image)
            view.tag = holder
        }
        else {
            view = convertView
            holder = convertView.tag as ViewHolder
        }

        val imageID = "${iconName}_${getItem(position)}"
        holder.iconName.text = getItem(position).toString().capitalize()
        holder.iconImage.setImageResource(view.resources.getIdentifier(imageID, "drawable", view.context.packageName))
        holder.iconImage.setColorFilter(R.color.fadedTextColor)

        return view
    }

    internal inner class ViewHolder {
        lateinit var iconName: TextView
        lateinit var iconImage: ImageView
    }

}