package com.rdall96.kitchentools.note

import android.arch.persistence.room.*

/*
 * NoteItemDao.kt
 * Project: Kitchen-Tools
 *
 * Author: Ricky Dall'Armellina
 * 3/29/19
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 * 
 */

@Dao
interface NoteItemDao {

    @Query("SELECT * FROM notes_db")
    fun getAll(): List<NoteItem>

    @Insert
    fun store(vararg value: NoteItem)

    @Update
    fun update(vararg value: NoteItem)

    @Delete
    fun remove(vararg value: NoteItem)

    @Query("DELETE FROM notes_db WHERE title = :value")
    fun removeByName(vararg value: String)

}