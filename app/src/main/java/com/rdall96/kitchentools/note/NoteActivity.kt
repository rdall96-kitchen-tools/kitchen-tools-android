package com.rdall96.kitchentools.note

import android.app.Activity
import android.app.AlertDialog
import android.arch.persistence.room.Room
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import com.rdall96.kitchentools.AppDatabase
import com.rdall96.kitchentools.R
import kotlinx.android.synthetic.main.activity_note.*
import kotlin.concurrent.thread

/*
 * NoteActivity.kt
 * Project: Kitchen-Tools
 *
 * Author: Ricky Dall'Armellina
 * 3/27/19
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 *
 */

@Suppress("NAME_SHADOWING", "DEPRECATION")
class NoteActivity : Fragment() {

    // UI Elements
    private lateinit var notesGrid: GridView
    private lateinit var frgActivity: FragmentActivity

    // Data
    private val notes: ArrayList<NoteItem> = arrayListOf()
    private lateinit var noteAdapter: NoteCellAdapter
    private var database: AppDatabase? = null


//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_note, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Read notes from database
        database = Room.databaseBuilder(this.context!!, AppDatabase::class.java, "notes_db").build()

        // Setup view
        notesGrid = view.findViewById(R.id.note_grid_view)
        noteAdapter = NoteCellAdapter(this.context!!, notes)
        notesGrid.adapter = noteAdapter

        notesGrid.setOnItemClickListener { _, view, position, _ ->
            val selectedNote = notes[position]

            val dialog= AlertDialog.Builder( this.context )
                .setTitle(selectedNote.title)
                .setMessage("What would you like to do?")
                .setPositiveButton("Edit") { _, _ ->
                    // Open edit task | request code 1
                    val editIntent = Intent(view.context, EditNoteActivity::class.java)
                    .putExtra("NOTE_POSITION", position)
                    .putExtra("NOTE_TITLE", selectedNote.title)
                    .putExtra("NOTE_TEXT", selectedNote.text)
                    .putExtra("NOTE_COLOR", selectedNote.color)
                    startActivityForResult(editIntent, 1)
                }
                .setNegativeButton("Delete") { _, _ ->
                    // Delete from table
                    notes.remove(selectedNote)
                    thread { database?.noteItemDao()?.remove(selectedNote) }
                    frgActivity.runOnUiThread { noteAdapter.notifyDataSetChanged() }
                }
                .create()
            dialog.setOnShowListener {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.primaryColor))
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(resources.getColor(R.color.secondaryColor))
            }
            dialog.show()
        }

        note_add_btn.setOnClickListener {
            val addIntent = Intent(it.context, EditNoteActivity::class.java)
            // request code 0
            startActivityForResult(addIntent, 0)
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        frgActivity = activity!!

        // Restore existing database data
        if(notes.isEmpty()) {
            thread {
                database?.noteItemDao()?.getAll()?.forEach { notes.add(it) }
                frgActivity.runOnUiThread { noteAdapter.notifyDataSetChanged() }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) { when (requestCode) {
            0 -> { // add note
                val title: String = data?.getStringExtra("NOTE_TITLE") ?: "Note"
                val text: String = data?.getStringExtra("NOTE_TEXT") ?: ""
                val color: Int = data!!.getIntExtra("NOTE_COLOR", 0xf4f5f7)
                val new_note = NoteItem(null, title, text, color)
                notes.add(new_note)
                thread { database?.noteItemDao()?.store(new_note) }
                frgActivity.runOnUiThread { noteAdapter.notifyDataSetChanged() }
            }
            1 -> { // edit note
                val title: String = data?.getStringExtra("NOTE_TITLE") ?: "Note"
                val text: String = data?.getStringExtra("NOTE_TEXT") ?: ""
                val color: Int = data!!.getIntExtra("NOTE_COLOR", 0xf4f5f7)
                val position: Int = data.getIntExtra("NOTE_POSITION", 0)
                notes[position].title = title
                notes[position].text = text
                notes[position].color = color
                thread { database?.noteItemDao()?.update(notes[position]) }
                frgActivity.runOnUiThread { noteAdapter.notifyDataSetChanged() }
            }
        }}
    }

    companion object {
        fun newInstance(): NoteActivity = NoteActivity()
    }
}
