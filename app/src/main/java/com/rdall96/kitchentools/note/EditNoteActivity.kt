package com.rdall96.kitchentools.note

import android.app.Activity
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import com.rdall96.kitchentools.R
import com.thebluealliance.spectrum.SpectrumDialog


/*
 * EditNoteActivity.kt
 * Project: Kitchen-Tools
 *
 * Author: Ricky Dall'Armellina
 * 3/27/19
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 *
 */

@Suppress("DEPRECATION")
class EditNoteActivity : AppCompatActivity() {

    // UI Elements
    private lateinit var noteTitleField: EditText
    private lateinit var noteTextField: EditText
    private lateinit var noteColorBtn: Button
    private lateinit var saveBtn: Button
    private lateinit var cancelBtn: Button

    // data
    private var noteColor: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_note)

        // Assign UI

        noteTitleField = findViewById(R.id.note_name_field)
        noteTextField = findViewById(R.id.note_text_field)
        noteColorBtn = findViewById(R.id.note_color_btn)
        saveBtn = findViewById(R.id.note_save_btn)
        cancelBtn = findViewById(R.id.note_cancel_btn)

        this.title = when (intent.getIntExtra("NOTE_POSITION", -1) > -1) {
            true -> "Edit Note"
            false -> "Add A New Note"
        }

        // Restore data in the views in in EDIT mode
        if(intent.getStringExtra("NOTE_TITLE") != null)
            noteTitleField.setText(intent.getStringExtra("NOTE_TITLE"))
        if(intent.getStringExtra("NOTE_TEXT") != null)
            noteTextField.setText(intent.getStringExtra("NOTE_TEXT"))
        noteColor = intent.getIntExtra("NOTE_COLOR", resources.getColor(R.color.note_white))
        ViewCompat.setBackgroundTintList(noteColorBtn, ColorStateList.valueOf(noteColor))


        // Buttons

        noteColorBtn.setOnClickListener {
            SpectrumDialog.Builder(it.context)
                .setColors(R.array.note_colors)
                .setTitle("Note color")
                .setDismissOnColorSelected(true)
                .setSelectedColor(-1) // hide selected color check mark
                .setOnColorSelectedListener { positiveResult, color ->
                    if(positiveResult) {
                        ViewCompat.setBackgroundTintList(noteColorBtn, ColorStateList.valueOf(color))
                        noteColor = color
                    }
                }
                .build().show(supportFragmentManager, "note_color_palette")
        }

        saveBtn.setOnClickListener {
            val resultIntent = Intent()
            resultIntent.putExtra("NOTE_TITLE",
                when (noteTitleField.text.toString()) {
                    "" -> "Note"
                    else -> noteTitleField.text.toString()
                })
            resultIntent.putExtra("NOTE_TEXT", noteTextField.text.toString())
            resultIntent.putExtra("NOTE_COLOR", noteColor)
            if (intent.getIntExtra("NOTE_POSITION", -1) > 0)
                resultIntent.putExtra("NOTE_POSITION", intent.getIntExtra("NOTE_POSITION", 0))
            setResult(Activity.RESULT_OK, resultIntent)
            finish()
        }

        cancelBtn.setOnClickListener {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }

    }

}
