package com.rdall96.kitchentools.note

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.util.*

/*
 * NoteItem.kt
 * Project: Kitchen-Tools
 *
 * Author: Ricky Dall'Armellina
 * 3/29/19
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 * 
 */

@Entity(tableName = "notes_db")
class NoteItem() {

    // Data

    @PrimaryKey(autoGenerate = false)
    var id: Int = UUID.randomUUID().toString().hashCode()

    @ColumnInfo(name = "title")
    var title: String = "Note"

    @ColumnInfo(name = "text")
    var text: String = ""

    @ColumnInfo(name = "color")
    var color: Int = 0xf4f5f7 // default white note


    // Initializer

    constructor(id: Int?, title: String, text: String, color: Int?) : this() {
        if (id != null) this.id = id
        this.title = title
        this.text = text
        if(color != null) this.color = color
    }

}
