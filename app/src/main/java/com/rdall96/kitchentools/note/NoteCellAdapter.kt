package com.rdall96.kitchentools.note

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.PorterDuff
import android.support.v4.view.TintableBackgroundView
import android.support.v4.view.ViewCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import com.rdall96.kitchentools.R

/*
 * NoteCellAdapter.kt
 * Project: Kitchen-Tools
 *
 * Author: Ricky Dall'Armellina
 * 3/29/19
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 * 
 */

class NoteCellAdapter(context: Context, private val dataSource: ArrayList<NoteItem>): BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        // Get view for row item
        val view: View
        val holder: ViewHolder
        val item: NoteItem = getItem(position) as NoteItem

        if(convertView == null) {

            view = inflater.inflate(R.layout.list_item_note, parent, false)
            holder = ViewHolder()

            // Prepare note properties
            holder.noteNameLabel = view.findViewById(R.id.note_item_title) as TextView
            holder.noteTextLabel = view.findViewById(R.id.note_item_text) as TextView
            holder.noteBackground = view.findViewById(R.id.note_item_view) as RelativeLayout

            view.tag = holder

        } else {
            view = convertView
            holder = convertView.tag as ViewHolder
        }

        // Assign values to UI elements in the cell
        holder.noteNameLabel.text = item.title
        holder.noteTextLabel.text = item.text
        ViewCompat.setBackgroundTintList(holder.noteBackground, ColorStateList.valueOf(item.color))

        return view
    }

    internal inner class ViewHolder {
        lateinit var noteNameLabel: TextView
        lateinit var noteTextLabel: TextView
        lateinit var noteBackground: RelativeLayout
    }

}