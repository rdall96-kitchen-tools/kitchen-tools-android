package com.rdall96.kitchentools

import android.app.*
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.graphics.Color
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.RingtoneManager
import android.net.Uri
import android.widget.Toast

/*
 * NotificationHelper.kt
 * Project: Kitchen-Tools
 *
 * Author: Ricky Dall'Armellina
 * 3/25/19
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 * 
 */

/* SEND A NOTIFICATION
 * define a notification helper         ->  private lateinit var mNotificationHelper: NotificationHelper
 * assign it a context                  ->  mNotificationHelper = NotificationHelper({context})
 * call notify and provide a unique ID  ->  mNotificationHelper.notify({ID}, mNotificationHelper.getNotificationTimer(title))
 */

internal class NotificationHelper (context: Context) : ContextWrapper(context) {

    // Data

    companion object {
        const val TIMER_DONE = "timer"
        lateinit var audioManager: AudioManager
    }

    private val mNotificationManager: NotificationManager by lazy {
        getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }


    // Initialization

    init {

        // Create the channel object with the unique ID TIMER_DONE
        val timerChannel = NotificationChannel(
            TIMER_DONE,
            "Timer Alarms",
            NotificationManager.IMPORTANCE_HIGH)

        // Configure the channel
        timerChannel.setBypassDnd(true)
        timerChannel.enableLights(true)
        timerChannel.lightColor = Color.BLUE
        timerChannel.enableVibration(true)
        timerChannel.vibrationPattern = longArrayOf(1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000)
        timerChannel.setSound(Uri.parse("android.resource://" + packageName + "/" + R.raw.ring),
            AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_ALARM)
                .build()
        )

        // Submit the notification channel object to the notification manager
        mNotificationManager.createNotificationChannel(timerChannel)

        // Set audio manager and check if ringer volume is on
        audioManager = this.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        if(audioManager.getStreamVolume(AudioManager.STREAM_ALARM) < 0.05f) {
            // Tell user to increase ringer volume.
            Toast.makeText(applicationContext, "Please enable your alarm ringer volume in order to receive timer sound notifications", Toast.LENGTH_LONG).show()
        }

    }


    // Methods

    fun getNotificationTimer(title: String): Notification.Builder {
        // Get a notification for a timer
        return Notification.Builder(applicationContext, TIMER_DONE)
            .setContentTitle(title)
            .setContentText("Time's up!")
            .setSmallIcon(R.drawable.ic_timers)
            .setTimeoutAfter(600000) // 10 minutes
            .setAutoCancel(false)
            .setContentIntent(pendingIntent)
    }

    private val pendingIntent: PendingIntent
        // Create an action to open the application's MainActivity when the notification is clicked -> PendingIntent
        get() {
            val openMainIntent = Intent(this, MainActivity::class.java)
            val stackBuilder = TaskStackBuilder.create(this)
            stackBuilder.addParentStack(MainActivity::class.java)
            stackBuilder.addNextIntent(openMainIntent)
            return stackBuilder.getPendingIntent(0, PendingIntent.FLAG_ONE_SHOT)
        }

    fun notify(id: Int, notification: Notification.Builder) {
        // Build notification and set last properties
        val item = notification.build()
        item.flags = Notification.FLAG_INSISTENT
        // Send the notification
        mNotificationManager.notify(id, item)
    }


}
