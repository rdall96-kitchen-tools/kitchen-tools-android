package com.rdall96.kitchentools

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.rdall96.kitchentools.note.NoteItem
import com.rdall96.kitchentools.note.NoteItemDao
import com.rdall96.kitchentools.timer.TimeKeeper
import com.rdall96.kitchentools.timer.TimeKeeperDao

/*
 * AppDatabase.kt
 * Project: Kitchen-Tools
 *
 * Author: Ricky Dall'Armellina
 * 3/27/19
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 * 
 */
 
@Database(entities = [(TimeKeeper::class), (NoteItem::class)], version = 1)
@TypeConverters(DatabaseConverters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun timeKeeperDao(): TimeKeeperDao
    abstract fun noteItemDao(): NoteItemDao
}