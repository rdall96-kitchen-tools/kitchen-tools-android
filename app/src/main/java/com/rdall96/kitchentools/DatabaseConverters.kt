package com.rdall96.kitchentools

import android.arch.persistence.room.TypeConverter
import android.os.CountDownTimer

/*
 * DatabaseConverters.kt
 * Project: Kitchen-Tools
 *
 * Author: Ricky Dall'Armellina
 * 3/27/19
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 * 
 */
 
class DatabaseConverters {

    @TypeConverter
    fun fromCountDownTimer(value: Int?): CountDownTimer? {
        return null
    }

    @TypeConverter
    fun timeToCountDownTimer(timer: CountDownTimer?): Int? {
        return null
    }

}