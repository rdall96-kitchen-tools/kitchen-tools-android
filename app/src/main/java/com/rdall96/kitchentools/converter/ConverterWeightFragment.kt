package com.rdall96.kitchentools.converter

import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

import com.rdall96.kitchentools.R

/*
 * ConverterWeightFragment.kt
 * Project: Kitchen-Tools
 *
 * Author: Ricky Dall'Armellina
 * 4/3/19
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 *
 */

class ConverterWeightFragment : Fragment() {

    // UI Elements
    private lateinit var firstMenu: Spinner
    private lateinit var secondMenu: Spinner
    private lateinit var firstTextField: EditText
    private lateinit var secondLabel: TextView
    private lateinit var swapBtn: Button

    // Data
    private enum class ConversionUnit(val value: String) {
        OZ("Ounce (oz)"),
        LB("Pound (lb)"),
        G("Gram (g)"),
        KG("Kilogram (Kg)")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_converter_weight, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Config UI
        firstMenu = view.findViewById(R.id.converter_weight_menu_first)
        secondMenu = view.findViewById(R.id.converter_weight_menu_second)
        firstTextField = view.findViewById(R.id.converter_weight_first)
        secondLabel = view.findViewById(R.id.converter_weight_second)
        swapBtn = view.findViewById(R.id.converter_swap_btn)

        firstTextField.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                updateConversionValue()
                ConverterActivity.savePreferences(firstTextField.editableText.toString(), resources.getString(R.string.pref_converter_number))
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun afterTextChanged(s: Editable?) {}
        })

        firstMenu.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                updateConversionValue()
                ConverterActivity.savePreferences(firstMenu.selectedItemPosition, resources.getString(R.string.pref_converter_unit_first))
            }
            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        secondMenu.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                updateConversionValue()
                ConverterActivity.savePreferences(secondMenu.selectedItemPosition, resources.getString(R.string.pref_converter_unit_second))
            }
            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        swapBtn.setOnClickListener {
            val firstUnit: Int = firstMenu.selectedItemPosition
            val secondUnit: Int = secondMenu.selectedItemPosition
            firstMenu.setSelection(secondUnit, true)
            secondMenu.setSelection(firstUnit, true)
        }

        // Restore last used values, if none, menuOne[0] menuTwo[1]
        firstTextField.setText(ConverterActivity.loadPreferencesString(resources.getString(R.string.pref_converter_number)), TextView.BufferType.EDITABLE)
        // Check if the value for the unit menus is in range of the amount of items in the menu
        val firstMenuSelection = ConverterActivity.loadPreferencesInt(resources.getString(R.string.pref_converter_unit_first)) ?: 0
        firstMenu.setSelection( when (firstMenuSelection) {
            in 0 until resources.getStringArray(R.array.converter_weight_items).count() -> firstMenuSelection
            else -> 0
        }, true)
        val secondMenuSelection = ConverterActivity.loadPreferencesInt(resources.getString(R.string.pref_converter_unit_second)) ?: 1
        secondMenu.setSelection( when (secondMenuSelection) {
            in 0 until resources.getStringArray(R.array.converter_weight_items).count() -> secondMenuSelection
            else -> 1
        }, true)

    }

    private fun updateConversionValue() {
        secondLabel.text = when (firstTextField.editableText.toString()) {
            "" -> ""
            else -> convert().format(2)
        }
    }

    private fun convert(): Double {
        return firstTextField.editableText.toString().toDouble() * conversionTableValue
            .getValue(ConversionUnit.values()[firstMenu.selectedItemPosition])
            .getValue(ConversionUnit.values()[secondMenu.selectedItemPosition])
    }

    private fun Double.format(digits: Int): String {
        return java.lang.String.format("%.${digits}f", this)
    }

    companion object {
        fun newInstance(): ConverterWeightFragment = ConverterWeightFragment()
    }

    private var conversionTableValue: Map<ConversionUnit, Map<ConversionUnit, Double>> = mapOf(
        ConversionUnit.OZ to mapOf(
            ConversionUnit.OZ to 1.0,
            ConversionUnit.LB to 0.0625,
            ConversionUnit.G to 28.34952,
            ConversionUnit.KG to 0.02835
        ),
        ConversionUnit.LB to mapOf(
            ConversionUnit.OZ to 16.0,
            ConversionUnit.LB to 1.0,
            ConversionUnit.G to 453.59237,
            ConversionUnit.KG to 0.45359
        ),
        ConversionUnit.G to mapOf(
            ConversionUnit.OZ to 0.03527,
            ConversionUnit.LB to 0.0022,
            ConversionUnit.G to 1.0,
            ConversionUnit.KG to 0.001
        ),
        ConversionUnit.KG to mapOf(
            ConversionUnit.OZ to 35.27396,
            ConversionUnit.LB to 220.46226,
            ConversionUnit.G to 1000.0,
            ConversionUnit.KG to 1.0
        )
    )

}
