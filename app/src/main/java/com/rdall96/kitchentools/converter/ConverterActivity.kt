package com.rdall96.kitchentools.converter

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Spinner
import com.rdall96.kitchentools.R
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import com.rdall96.kitchentools.IconSpinnerAdapter


/*
 * ConverterActivity.kt
 * Project: Kitchen-Tools
 *
 * Author: Ricky Dall'Armellina
 * 3/27/19
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 *
 */

class ConverterActivity : Fragment() {

    // UI Elements
    private lateinit var typeMenu: Spinner
    private lateinit var frgActivity: FragmentActivity

    // Data
    private lateinit var typeMenuAdapter: IconSpinnerAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Setup preferences
        preferences = context!!.getSharedPreferences("KTConverter", 0) // mode 0 for private mode
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_converter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Initialize UI
        typeMenu = view.findViewById(com.rdall96.kitchentools.R.id.converter_dropdown_menu)
        val typeMenuNames = resources.getStringArray(R.array.converter_type_items)
        typeMenuAdapter = IconSpinnerAdapter(this.context!!, typeMenuNames, "ic")
        typeMenu.adapter = typeMenuAdapter
        typeMenu.onItemSelectedListener = object: OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                when (typeMenuAdapter.getItem(pos) as String) {
                    "volume" -> loadFragment(ConverterVolumeFragment.newInstance())
                    "weight" -> loadFragment(ConverterWeightFragment.newInstance())
                    "temperature" -> loadFragment(ConverterTemperatureFragment.newInstance())
                    "length" -> loadFragment(ConverterLengthFragment.newInstance())
                }
                // Store used converter type
                savePreferences(pos, resources.getString(R.string.pref_converter_type_last))
            }
            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
        // Set the last used converter type
        typeMenu.setSelection(loadPreferencesInt(resources.getString(R.string.pref_converter_type_last)) ?: 0, true)
    }

    @SuppressLint("CommitPrefEdits")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        frgActivity = activity!!
    }

    private fun loadFragment(fragment: Fragment) {
        val transaction = frgActivity.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.converter_fragment_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    companion object {

        fun newInstance(): ConverterActivity = ConverterActivity()

        lateinit var preferences: SharedPreferences

        fun savePreferences(data: String, forKey: String) {
            val editor = preferences.edit()
            editor.putString(forKey, data)
            editor.apply()
        }

        fun savePreferences(data: Int, forKey: String) {
            val editor = preferences.edit()
            editor.putInt(forKey, data)
            editor.apply()
        }

        fun loadPreferencesString(forKey: String): String? {
            return preferences.getString(forKey, null)
        }

        fun loadPreferencesInt(forKey: String): Int? {
            val value = preferences.getInt(forKey, -1)
            return when (value) {
                -1 -> null
                else -> value
            }
        }

        fun clearPreferences() {
            val editor = preferences.edit()
            editor.clear()
            editor.apply()
        }

    }

}
