package com.rdall96.kitchentools.converter

import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

import com.rdall96.kitchentools.R

/*
 * ConverterTemperatureFragment.kt
 * Project: Kitchen-Tools
 *
 * Author: Ricky Dall'Armellina
 * 4/3/19
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 *
 */

class ConverterTemperatureFragment : Fragment() {

    // UI Elements
    private lateinit var firstMenu: Spinner
    private lateinit var secondMenu: Spinner
    private lateinit var firstTextField: EditText
    private lateinit var secondLabel: TextView
    private lateinit var swapBtn: Button

    // Data
    private enum class ConversionUnit(val value: String) {
        C("Celsius (C)"),
        F("Fahrenheit (F)")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_converter_temperature, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Config UI
        firstMenu = view.findViewById(R.id.converter_temperature_menu_first)
        secondMenu = view.findViewById(R.id.converter_temperature_menu_second)
        firstTextField = view.findViewById(R.id.converter_temperature_first)
        secondLabel = view.findViewById(R.id.converter_temperature_second)
        swapBtn = view.findViewById(R.id.converter_swap_btn)

        firstTextField.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                updateConversionValue()
                ConverterActivity.savePreferences(firstTextField.editableText.toString(), resources.getString(R.string.pref_converter_number))
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun afterTextChanged(s: Editable?) {}
        })

        firstMenu.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                updateConversionValue()
                ConverterActivity.savePreferences(firstMenu.selectedItemPosition, resources.getString(R.string.pref_converter_unit_first))
            }
            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        secondMenu.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                updateConversionValue()
                ConverterActivity.savePreferences(secondMenu.selectedItemPosition, resources.getString(R.string.pref_converter_unit_second))
            }
            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        swapBtn.setOnClickListener {
            val firstUnit: Int = firstMenu.selectedItemPosition
            val secondUnit: Int = secondMenu.selectedItemPosition
            firstMenu.setSelection(secondUnit, true)
            secondMenu.setSelection(firstUnit, true)
        }

        // Restore last used values, if none, menuOne[0] menuTwo[1]
        firstTextField.setText(ConverterActivity.loadPreferencesString(resources.getString(R.string.pref_converter_number)), TextView.BufferType.EDITABLE)
        // Check if the value for the unit menus is in range of the amount of items in the menu
        val firstMenuSelection = ConverterActivity.loadPreferencesInt(resources.getString(R.string.pref_converter_unit_first)) ?: 0
        firstMenu.setSelection( when (firstMenuSelection) {
            in 0 until resources.getStringArray(R.array.converter_temperature_items).count() -> firstMenuSelection
            else -> 0
        }, true)
        val secondMenuSelection = ConverterActivity.loadPreferencesInt(resources.getString(R.string.pref_converter_unit_second)) ?: 1
        secondMenu.setSelection( when (secondMenuSelection) {
            in 0 until resources.getStringArray(R.array.converter_temperature_items).count() -> secondMenuSelection
            else -> 1
        }, true)

    }

    private fun updateConversionValue() {
        secondLabel.text = when (firstTextField.editableText.toString()) {
            "" -> ""
            else -> convert().format(2)
        }
    }

    private fun convert(): Double {
        val number = firstTextField.editableText.toString().toDouble()
        return when (ConversionUnit.values()[firstMenu.selectedItemPosition]) {
            ConversionUnit.C -> when (ConversionUnit.values()[secondMenu.selectedItemPosition]) {
                ConversionUnit.C -> number
                ConversionUnit.F -> (number * 9.0 / 5.0) + 32
            }
            ConversionUnit.F -> when (ConversionUnit.values()[secondMenu.selectedItemPosition]) {
                ConversionUnit.C -> (number - 32.0) * (5.0 / 9.0)
                ConversionUnit.F -> number
            }
        }
    }

    private fun Double.format(digits: Int): String {
        return java.lang.String.format("%.${digits}f", this)
    }

    companion object {
        fun newInstance(): ConverterTemperatureFragment = ConverterTemperatureFragment()
    }

}
