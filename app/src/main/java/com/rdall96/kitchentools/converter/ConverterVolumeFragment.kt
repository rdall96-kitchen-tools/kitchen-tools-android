package com.rdall96.kitchentools.converter

import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

import com.rdall96.kitchentools.R

/*
 * ConverterVolumeFragment.kt
 * Project: Kitchen-Tools
 *
 * Author: Ricky Dall'Armellina
 * 4/3/19
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 *
 */

class ConverterVolumeFragment : Fragment() {

    // UI Elements
    private lateinit var firstMenu: Spinner
    private lateinit var secondMenu: Spinner
    private lateinit var firstTextField: EditText
    private lateinit var secondLabel: TextView
    private lateinit var swapBtn: Button

    // Data
    private enum class ConversionUnit(val value: String) {
        TSP("Teaspoon (tsp)"),
        TBSP("Tablespoon (tbsp)"),
        CUP("Cup"),
        FLOZ("Fluid Ounce (fl oz)"),
        ML("Milliliter (ml)"),
        L("Liter (L)"),
        PT("Pint (pt)"),
        QT("Quart (qt)")
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_converter_volume, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Config UI
        firstMenu = view.findViewById(R.id.converter_volume_menu_first)
        secondMenu = view.findViewById(R.id.converter_volume_menu_second)
        firstTextField = view.findViewById(R.id.converter_volume_first)
        secondLabel = view.findViewById(R.id.converter_volume_second)
        swapBtn = view.findViewById(R.id.converter_swap_btn)

        firstTextField.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                updateConversionValue()
                ConverterActivity.savePreferences(firstTextField.editableText.toString(), resources.getString(R.string.pref_converter_number))
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun afterTextChanged(s: Editable?) {}
        })

        firstMenu.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                updateConversionValue()
                ConverterActivity.savePreferences(firstMenu.selectedItemPosition, resources.getString(R.string.pref_converter_unit_first))
            }
            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        secondMenu.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                updateConversionValue()
                ConverterActivity.savePreferences(secondMenu.selectedItemPosition, resources.getString(R.string.pref_converter_unit_second))
            }
            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        swapBtn.setOnClickListener {
            val firstUnit: Int = firstMenu.selectedItemPosition
            val secondUnit: Int = secondMenu.selectedItemPosition
            firstMenu.setSelection(secondUnit, true)
            secondMenu.setSelection(firstUnit, true)
        }

        // Restore last used values, if none, menuOne[0] menuTwo[1]
        firstTextField.setText(ConverterActivity.loadPreferencesString(resources.getString(R.string.pref_converter_number)), TextView.BufferType.EDITABLE)
        // Check if the value for the unit menus is in range of the amount of items in the menu
        val firstMenuSelection = ConverterActivity.loadPreferencesInt(resources.getString(R.string.pref_converter_unit_first)) ?: 0
        firstMenu.setSelection( when (firstMenuSelection) {
            in 0 until resources.getStringArray(R.array.converter_volume_items).count() -> firstMenuSelection
            else -> 0
        }, true)
        val secondMenuSelection = ConverterActivity.loadPreferencesInt(resources.getString(R.string.pref_converter_unit_second)) ?: 1
        secondMenu.setSelection( when (secondMenuSelection) {
            in 0 until resources.getStringArray(R.array.converter_volume_items).count() -> secondMenuSelection
            else -> 1
        }, true)

    }

    private fun updateConversionValue() {
        secondLabel.text = when (firstTextField.editableText.toString()) {
            "" -> ""
            else -> convert().format(2)
        }
    }

    private fun convert(): Double {
        return firstTextField.editableText.toString().toDouble() * conversionTableValue
            .getValue(ConversionUnit.values()[firstMenu.selectedItemPosition])
            .getValue(ConversionUnit.values()[secondMenu.selectedItemPosition])
    }

    private fun Double.format(digits: Int): String {
        return java.lang.String.format("%.${digits}f", this)
    }

    companion object {
        fun newInstance(): ConverterVolumeFragment = ConverterVolumeFragment()
    }

    private val conversionTableValue: Map<ConversionUnit, Map<ConversionUnit, Double>> = mapOf(
        ConversionUnit.TSP to mapOf(
            ConversionUnit.TSP to 1.0,
            ConversionUnit.TBSP to 0.33333,
            ConversionUnit.CUP to 0.02,
            ConversionUnit.FLOZ to 0.16667,
            ConversionUnit.ML to 5.0,
            ConversionUnit.L to 0.005,
            ConversionUnit.PT to 0.0088,
            ConversionUnit.QT to 0.00528
        ),
        ConversionUnit.TBSP to mapOf(
            ConversionUnit.TSP to 3.0,
            ConversionUnit.TBSP to 1.0,
            ConversionUnit.CUP to 0.06,
            ConversionUnit.FLOZ to 0.5,
            ConversionUnit.ML to 15.0,
            ConversionUnit.L to 0.015,
            ConversionUnit.PT to 0.02639,
            ConversionUnit.QT to 0.01585
        ),
        ConversionUnit.CUP to mapOf(
            ConversionUnit.TSP to 50.0,
            ConversionUnit.TBSP to 16.66667,
            ConversionUnit.CUP to 1.0,
            ConversionUnit.FLOZ to 8.33333,
            ConversionUnit.ML to 250.0,
            ConversionUnit.L to 0.25,
            ConversionUnit.PT to 0.43994,
            ConversionUnit.QT to 0.26417
        ),
        ConversionUnit.FLOZ to mapOf(
            ConversionUnit.TSP to 6.0,
            ConversionUnit.TBSP to 2.0,
            ConversionUnit.CUP to 0.12,
            ConversionUnit.FLOZ to 1.0,
            ConversionUnit.ML to 30.0,
            ConversionUnit.L to 0.03,
            ConversionUnit.PT to 0.05279,
            ConversionUnit.QT to 0.0317
        ),
        ConversionUnit.ML to mapOf(
            ConversionUnit.TSP to 0.2,
            ConversionUnit.TBSP to 0.06667,
            ConversionUnit.CUP to 0.004,
            ConversionUnit.FLOZ to 0.03333,
            ConversionUnit.ML to 1.0,
            ConversionUnit.L to 0.001,
            ConversionUnit.PT to 0.00176,
            ConversionUnit.QT to 0.00106
        ),
        ConversionUnit.L to mapOf(
            ConversionUnit.TSP to 200.0,
            ConversionUnit.TBSP to 66.66667,
            ConversionUnit.CUP to 4.0,
            ConversionUnit.FLOZ to 33.33333,
            ConversionUnit.ML to 1000.0,
            ConversionUnit.L to 1.0,
            ConversionUnit.PT to 1.75975,
            ConversionUnit.QT to 1.05669
        ),
        ConversionUnit.PT to mapOf(
            ConversionUnit.TSP to 113.65225,
            ConversionUnit.TBSP to 37.88408,
            ConversionUnit.CUP to 2.27304,
            ConversionUnit.FLOZ to 18.94204,
            ConversionUnit.ML to 568.26125,
            ConversionUnit.L to 0.56826,
            ConversionUnit.PT to 1.0,
            ConversionUnit.QT to 0.60047
        ),
        ConversionUnit.QT to mapOf(
            ConversionUnit.TSP to 189.27059,
            ConversionUnit.TBSP to 63.0902,
            ConversionUnit.CUP to 3.78541,
            ConversionUnit.FLOZ to 31.5451,
            ConversionUnit.ML to 946.35294,
            ConversionUnit.L to 0.94635,
            ConversionUnit.PT to 1.66535,
            ConversionUnit.QT to 1.0
        )
    )

}
