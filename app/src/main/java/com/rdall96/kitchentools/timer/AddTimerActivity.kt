package com.rdall96.kitchentools.timer

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TextInputLayout
import android.widget.*
import com.rdall96.kitchentools.IconSpinnerAdapter
import com.rdall96.kitchentools.R

/*
 * AddTimerActivity.kt
 * Project: Kitchen-Tools
 *
 * Author: Ricky Dall'Armellina
 * 3/20/19
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 *
 */

class AddTimerActivity : AppCompatActivity() {

    // UI Elements
    private lateinit var timerNameLabel: TextInputLayout
    private lateinit var timePicker: TimePicker
    private lateinit var autoStartSwitch: Switch
    private lateinit var iconSpinner: Spinner
    private lateinit var saveBtn: Button
    private lateinit var  cancelBtn: Button

    // Data
    private lateinit var iconNames: Array<String>
    private lateinit var iconAdapter: IconSpinnerAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_timer)

        // Config UI

        timerNameLabel = findViewById(R.id.timer_name_field)
        if(intent.getStringExtra("TIMER_NAME") != null)
            timerNameLabel.editText!!.setText(intent.getStringExtra("TIMER_NAME"), TextView.BufferType.EDITABLE)
        timerNameLabel.hint = intent.getStringExtra("TIMER_NAME_HINT") ?: "Timer"

        timePicker = findViewById(R.id.timer_picker)
        timePicker.setIs24HourView(true)
        timePicker.hour = intent.getIntExtra("TIMER_HOUR", 0)
        timePicker.minute = intent.getIntExtra("TIMER_MINUTE", 1)

        autoStartSwitch = findViewById(R.id.timer_autostart_switch)
        if (intent.getIntExtra("TIMER_POSITION", -1) > -1)
            autoStartSwitch.isEnabled = false

        iconSpinner = findViewById(R.id.timer_icon_picker)
        iconNames = resources.getStringArray(R.array.timer_icon_items)
        iconAdapter = IconSpinnerAdapter(this, iconNames, "ic_kt_food")
        iconSpinner.adapter = iconAdapter
        iconSpinner.setSelection(intent.getIntExtra("TIMER_ICON", 0))

        saveBtn = findViewById(R.id.timer_save_btn)
        cancelBtn = findViewById(R.id.timer_cancel_btn)

        this.title = when (intent.getIntExtra("TIMER_POSITION", -1) > -1) {
            true -> "Edit Timer"
            false -> "Add A New Timer"
        }


        // Action buttons
        saveBtn.setOnClickListener {
            val resultIntent = Intent()
            resultIntent.putExtra("TIMER_NAME",
                when (timerNameLabel.editText?.text.toString().isBlank()) {
                    true -> timerNameLabel.hint
                    else -> timerNameLabel.editText!!.text.toString()
                }
            )
            resultIntent.putExtra("TIMER_HOUR", timePicker.hour)
            resultIntent.putExtra("TIMER_MINUTE", timePicker.minute)
            resultIntent.putExtra("TIMER_START", autoStartSwitch.isChecked)
            resultIntent.putExtra("TIMER_ICON", iconSpinner.selectedItemPosition)
            if(intent.getIntExtra("TIMER_POSITION", -1) > 0)
                resultIntent.putExtra("TIMER_POSITION", intent.getIntExtra("TIMER_POSITION", 0))
            setResult(Activity.RESULT_OK, resultIntent)
            finish()
        }

        cancelBtn.setOnClickListener {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }


    }


}
