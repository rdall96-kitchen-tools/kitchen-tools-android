package com.rdall96.kitchentools.timer

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.Spinner
import com.rdall96.kitchentools.R

class AddTimeDialog : Activity() {

    // UI Elements
    private lateinit var addTimeSpinner: Spinner
    private lateinit var cancelBtn: Button
    private lateinit var addBtn: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Set UI

        setContentView(R.layout.activity_add_time_dialog)
        title = intent.getStringExtra("TIMER_NAME") ?: "Timer"
        // don't dismiss if clicked outside, since this view is a dialog. Use cancel button instead
        this.setFinishOnTouchOutside(false)
        addTimeSpinner = findViewById(R.id.timer_add_time_spinner)
        cancelBtn = findViewById(R.id.timer_add_time_cancel_btn)
        addBtn = findViewById(R.id.timer_add_time_add_btn)

        cancelBtn.setOnClickListener {
            setResult(RESULT_CANCELED)
            finish()
        }

        addBtn.setOnClickListener {
            val time: Int = resources.getStringArray(R.array.timer_add_time_values)[addTimeSpinner.selectedItemPosition]
                .substringBefore(" ")
                .toInt() * 60 // multiply by 60 to turn a single integer into minutes
            val resultIntent = Intent()
            resultIntent.putExtra("TIMER_POSITION", intent.getIntExtra("TIMER_POSITION", -1))
            resultIntent.putExtra("TIMER_ADD_TIME", time)
            setResult(RESULT_OK, resultIntent)
            finish()
        }

    }

}
