package com.rdall96.kitchentools.timer

import android.annotation.SuppressLint
import android.content.Context
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.rdall96.kitchentools.NotificationHelper
import com.rdall96.kitchentools.R
import java.util.*

/*
 * TimerCellAdapter.kt
 * Project: Kitchen-Tools
 *
 * Author: Ricky Dall'Armellina
 * 3/20/19
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 * 
 */

@Suppress("DEPRECATION")
class TimerCellAdapter(context: Context, private val dataSource: ArrayList<TimeKeeper>): BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private lateinit var mNotificationHelper: NotificationHelper

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @SuppressLint("SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val view: View
        val holder: ViewHolder
        val item: TimeKeeper = TimeMaster.getTimerAt(position)

        if(convertView == null) {

            view = inflater.inflate(R.layout.list_item_timer, parent, false)
            holder = ViewHolder()

            // Prepare timer properties
            holder.timerNameLabel = view.findViewById(R.id.timer_item_title) as TextView
            holder.timeLeftLabel = view.findViewById(R.id.timer_item_time) as TextView
            holder.timerPauseBtn = view.findViewById(R.id.timer_item_button) as Button
            holder.timerIcon = view.findViewById(R.id.timer_item_icon) as ImageView

            view.tag = holder

        } else {
            view = convertView
            holder = convertView.tag as ViewHolder
        }

        mNotificationHelper = NotificationHelper(view.context)

        // Assign values to UI elements in the cell
        holder.timerNameLabel.text = item.name
        holder.timeLeftLabel.text = item.formattedTime()
        val imageID = "ic_kt_food_${view.resources.getStringArray(R.array.timer_icon_items)[item.icon]}"
        holder.timerIcon.setImageResource(view.resources.getIdentifier(imageID, "drawable", view.context.packageName))
        when (item.isRunning) {
            true -> {
                holder.timeLeftLabel.setTextColor(view.resources.getColor(R.color.primaryDarkColor))
                holder.timerPauseBtn.foreground = view.resources.getDrawable(R.drawable.ic_pause)
            }
            false -> {
                holder.timeLeftLabel.setTextColor(view.resources.getColor(R.color.fadedTextColor))
                holder.timerPauseBtn.foreground = view.resources.getDrawable(R.drawable.ic_play)
            }
        }

        holder.timerPauseBtn.setOnClickListener { when (item.isRunning) {
            false -> {
                if(item.isDone()) { // Reset timer
                    item.reset()
                    holder.timerPauseBtn.foreground = view.resources.getDrawable(R.drawable.ic_play)
                    holder.timeLeftLabel.setTextColor(view.resources.getColor(R.color.fadedTextColor))
                    holder.timeLeftLabel.text = item.formattedTime()
                }
                else { // Resume/Start the timer
                    item.start()
                    holder.timerPauseBtn.foreground = view.resources.getDrawable(R.drawable.ic_pause)
                    holder.timeLeftLabel.setTextColor(view.resources.getColor(R.color.primaryDarkColor))
                    holder.timerUpdater = object : CountDownTimer((item.getTimeLeft().toLong() * 1000), 500) {
                        override fun onTick(millisUntilFinished: Long) {
                            // Update label
                            holder.timeLeftLabel.text = item.formattedTime()
                        }
                        override fun onFinish() { if (item.isDone()) {
                            // Write done on the label
                            holder.timeLeftLabel.setTextColor(view.resources.getColor(R.color.doneColor))
                            holder.timeLeftLabel.text = "DONE"
                            holder.timerPauseBtn.foreground = view.resources.getDrawable(R.drawable.ic_reset)
                            // Send notification
                            mNotificationHelper.notify(item.getID(), mNotificationHelper.getNotificationTimer(item.name)
                            )
                        }}
                    }.start()
                }
            }
            true -> {
                // Stop the timer
                item.pause()
                holder.timerPauseBtn.foreground = view.resources.getDrawable(R.drawable.ic_play)
                holder.timeLeftLabel.setTextColor(view.resources.getColor(R.color.fadedTextColor))
                holder.timerUpdater!!.cancel()
                holder.timerUpdater = null
            }}
        }

        // Create timer to update label if timer is already running
        if(item.isRunning) {
            item.start()
            holder.timerUpdater = object : CountDownTimer((item.getTimeLeft().toLong() * 1000), 500) {
                override fun onTick(millisUntilFinished: Long) {
                    // Update label
                    holder.timeLeftLabel.text = item.formattedTime()
                }
                override fun onFinish() { if (item.isDone()) {
                    // Write done on the label
                    holder.timeLeftLabel.setTextColor(view.resources.getColor(R.color.doneColor))
                    holder.timeLeftLabel.text = "DONE"
                    holder.timerPauseBtn.foreground = view.resources.getDrawable(R.drawable.ic_reset)
                    // Send notification
                    mNotificationHelper.notify(item.getID(), mNotificationHelper.getNotificationTimer(item.name))
                }}
            }.start()
        }

        return view
    }

    internal inner class ViewHolder {
        lateinit var timerNameLabel: TextView
        lateinit var timeLeftLabel: TextView
        lateinit var timerPauseBtn: Button
        lateinit var timerIcon: ImageView
        var timerUpdater: CountDownTimer? = null
    }


}
