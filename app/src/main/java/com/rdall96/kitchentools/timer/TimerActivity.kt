package com.rdall96.kitchentools.timer

import android.app.Activity
import android.arch.persistence.room.Room
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.view.*
import android.widget.ListView
import com.rdall96.kitchentools.AppDatabase
import com.rdall96.kitchentools.R
import kotlinx.android.synthetic.main.activity_timer.*
import kotlin.concurrent.thread
import android.app.AlertDialog
import java.sql.Time

/*
 * TimerActivity.kt
 * Project: Kitchen-Tools
 *
 * Author: Ricky Dall'Armellina
 * 3/20/19
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 *
 */

@Suppress("NAME_SHADOWING", "DEPRECATION")
class TimerActivity : Fragment() {

    // UI Elements
    private lateinit var timerListView: ListView
    private lateinit var frgActivity: FragmentActivity

    // Data
    private lateinit var timerAdapter: TimerCellAdapter
    private var database: AppDatabase? = null


    companion object {
        fun newInstance(): TimerActivity = TimerActivity()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_timer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Check for database
        database = Room.databaseBuilder(this.context!!, AppDatabase::class.java, "timer_list_db").build()

        // Add timer button
        timer_add_btn.setOnClickListener {
            val addIntent = Intent(it.context, AddTimerActivity::class.java)
            addIntent.putExtra("TIMER_NAME_HINT", "Timer ${timerListView.adapter.count + 1}")
            startActivityForResult(addIntent, 0)
        }

        // Setup ListView
        timerListView = view.findViewById(R.id.timer_list_view)
        timerAdapter = TimerCellAdapter(this.context!!, TimeMaster.getAllTimers())
        timerListView.adapter = timerAdapter

        timerListView.setOnItemClickListener { _, view, position, _ ->
            val selectedTimer = TimeMaster.getTimerAt(position)

            val dialog= AlertDialog.Builder( this.context )
                .setTitle("Timer: ${selectedTimer.name}")
                .setMessage("What would you like to do?")
                .setPositiveButton("Edit") { _, _ ->
                    // Open edit task
                    val editIntent = Intent(view.context, AddTimerActivity::class.java)
                    .putExtra("TIMER_POSITION", position)
                    .putExtra("TIMER_NAME", selectedTimer.name)
                    .putExtra("TIMER_NAME_HINT", "Timer ${(position + 1)}")
                    .putExtra("TIMER_HOUR", selectedTimer.getHours())
                    .putExtra("TIMER_MINUTE", selectedTimer.getMinutes())
                    .putExtra("TIMER_ICON", selectedTimer.icon)
                    startActivityForResult(editIntent, 1)
                }
                .setNegativeButton("Delete") { _, _ ->
                    // Delete from table
                    TimeMaster.removeTimerAt(position)
                    thread { database!!.timeKeeperDao().remove(selectedTimer) }
                    frgActivity.runOnUiThread { timerAdapter.notifyDataSetChanged() }
                }
                .setNeutralButton("Add Time") {_, _ ->
                    // Add time to running timer - Start activity with  dialog to add time
                    val addTimeIntent = Intent(view.context, AddTimeDialog::class.java)
                        .putExtra("TIMER_POSITION", position)
                        .putExtra("TIMER_NAME", TimeMaster.getTimerAt(position).name)
                    startActivityForResult(addTimeIntent, 2)
                }
                .create()
            dialog.setOnShowListener {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.primaryColor))
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(resources.getColor(R.color.secondaryColor))
                dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(resources.getColor(R.color.primaryColor))
            }
            dialog.show()
        }

    }

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        frgActivity = activity!!

        // Restore existing database data
        if(TimeMaster.getAllTimers().isEmpty()) {
            thread {
                database?.timeKeeperDao()?.getAll()?.forEach { TimeMaster.addNewTimer(it.id, it.name, it.time, it.isRunning, it.icon) }
                frgActivity.runOnUiThread { timerAdapter.notifyDataSetChanged() }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) { when (requestCode) {
            0 -> { // add timer
                val name: String = data!!.getStringExtra("TIMER_NAME") ?: "Timer"
                val h: Int = (data.getIntExtra("TIMER_HOUR", 0)) * 3600
                val m: Int = (data.getIntExtra("TIMER_MINUTE", 0)) * 60
                val time: Int = h + m
                val start: Boolean = data.getBooleanExtra("TIMER_START", false)
                val icon: Int = data.getIntExtra("TIMER_ICON", 0)
                TimeMaster.addNewTimer(null, name, time, start, icon)
                thread { database?.timeKeeperDao()?.store(TimeKeeper(null, name, time, false, icon)) }
                frgActivity.runOnUiThread { timerAdapter.notifyDataSetChanged() }
            }
            1 -> { // edit timer
                val name: String = data!!.getStringExtra("TIMER_NAME") ?: "Timer"
                val h: Int = (data.getIntExtra("TIMER_HOUR", 0)) * 3600
                val m: Int = (data.getIntExtra("TIMER_MINUTE", 0)) * 60
                val time: Int = h + m
                val position: Int = data.getIntExtra("TIMER_POSITION", 0)
                val icon: Int = data.getIntExtra("TIMER_ICON", 0)
                val newPos = TimeMaster.updateTimerAt(position, name, time, icon)
                thread { database!!.timeKeeperDao().update(TimeMaster.getTimerAt(newPos)) }
                frgActivity.runOnUiThread { timerAdapter.notifyDataSetChanged() }
            }
            2 -> { // add time to timer
                val position: Int = data!!.getIntExtra("TIMER_POSITION", -1)
                val timeToAdd: Int = data.getIntExtra("TIMER_ADD_TIME", 0)
                TimeMaster.getTimerAt(position).addTime(timeToAdd)
                // Pause and restart timer to force UI to invalidate timerUpdater object
                TimeMaster.getTimerAt(position).pause()
                frgActivity.runOnUiThread { timerAdapter.notifyDataSetChanged() }
                TimeMaster.getTimerAt(position).start()
                thread { database!!.timeKeeperDao().update(TimeMaster.getTimerAt(position)) }
                frgActivity.runOnUiThread { timerAdapter.notifyDataSetChanged() }
            }
        }}
    }


}
