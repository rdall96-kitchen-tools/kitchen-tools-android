package com.rdall96.kitchentools.timer

import android.arch.persistence.room.*
import android.os.CountDownTimer
import java.util.*


/*
 * TimeKeeper.kt
 * Project: Kitchen-Tools
 *
 * Author: Ricky Dall'Armellina
 * 3/20/19
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 * 
 */

@Entity(tableName = "timer_list_db")
class TimeKeeper() {

    // Data

    @PrimaryKey(autoGenerate = false)
    var id: Int = UUID.randomUUID().toString().hashCode()

    @ColumnInfo(name = "name")
    var name: String = "Timer"

    @ColumnInfo(name = "time")
    var time: Int = 0 // time left in seconds

    @ColumnInfo(name="default_time")
    var startTime: Int = 0 // initial start time in seconds

    @ColumnInfo(name="timer_object")
    var timer: CountDownTimer? = null

    @ColumnInfo(name = "is_running")
    var isRunning: Boolean = false

    @ColumnInfo(name = "icon")
    var icon: Int = 0


    // Initializer

    constructor(id: Int?, name: String?, time: Int, start: Boolean?, icon: Int?) : this() {

        if (id != null) this.id = id
        this.name = name ?: "Timer"
        this.time = time
        this.startTime = time

        if(start!!) this.start()
        if(icon != null) this.icon = icon

    }


    // Methods

    fun start() {
        if(!isRunning) {
            isRunning = true
            timer?.cancel()
            timer = object : CountDownTimer((time.toLong() * 1000), 1000) {
                override fun onTick(millisUntilFinished: Long) { tick() }
                override fun onFinish() { timerDone() }
            }.start()
        }
    }

    fun stop() {
        isRunning = false
        timer?.cancel()
        timer = null
        time = 0
    }

    fun pause() {
        isRunning = false
        timer?.cancel()
        timer = null
    }

    fun reset() {
        time = startTime
    }

    private fun tick() {
        when {
            time <= 0 -> stop()
            else -> time -= 1
        }
    }

    fun addTime(seconds: Int) {
        time += seconds
    }

    fun isDone(): Boolean {
        return when {
            time <= 0 -> true
            else -> false
        }
    }

    private fun timerDone() {
        isRunning = false
        timer?.cancel()
        timer = null
    }

    fun formattedTime(): String {
        return when (isDone()) {
            false -> {
                val hours = time / 3600
                val minutes = time / 60 % 60
                val seconds = time % 60
                "${hours.format()}:${minutes.format()}:${seconds.format()}"
            }
            true -> "DONE"
        }
    }

    private fun Int.format(): String {
        return if (this.toString().count() > 1) { this.toString() }
        else { "0${this}" }
    }

    fun getDefaultTime(): Int { return startTime }

    fun getTimeLeft(): Int { return time }

    fun setNewTime(time: Int) {
        this.time = time
        this.startTime = time
    }

    fun getHours(): Int { return startTime / 3600 }

    fun getMinutes(): Int { return startTime / 60 % 60 }

    fun getID(): Int { return this.id }

}
