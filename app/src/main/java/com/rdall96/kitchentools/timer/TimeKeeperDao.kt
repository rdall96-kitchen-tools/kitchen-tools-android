package com.rdall96.kitchentools.timer

import android.arch.persistence.room.*

/*
 * TimeKeeperDao.kt
 * Project: Kitchen-Tools
 *
 * Author: Ricky Dall'Armellina
 * 3/27/19
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 * 
 */

@Dao
interface TimeKeeperDao {

    @Query("SELECT * FROM timer_list_db")
    fun getAll(): List<TimeKeeper>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun store(vararg value: TimeKeeper)

    @Update
    fun update(vararg value: TimeKeeper)

    @Delete
    fun remove(vararg value: TimeKeeper)

}