package com.rdall96.kitchentools.timer

import com.rdall96.kitchentools.AppDatabase

/*
 * TimeMaster.kt
 * Project: Kitchen-Tools
 *
 * Author: Ricky Dall'Armellina
 * 4/2/19
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 * 
 */

object TimeMaster {

    // Data

    private var timers: MutableList<TimeKeeper> = mutableListOf()

    // Methods

    fun getAllTimers(): ArrayList<TimeKeeper> {
        return this.timers as ArrayList<TimeKeeper>
    }

    fun getTimerAt(position: Int): TimeKeeper {
        return this.timers[position]
    }

    fun addNewTimer(id: Int?, name: String, time: Int, start: Boolean, icon: Int?) {
        this.timers.add(TimeKeeper(id, name, time, start, icon))
        sortTimers()
    }

    fun updateTimerAt(position: Int, name: String, time: Int, icon: Int): Int {
        val itemID = this.timers[position].id
        this.timers[position].name = name
        this.timers[position].setNewTime(time)
        this.timers[position].icon = icon
        this.timers[position].reset()
        sortTimers()
        return this.timers.indexOf(this.timers.filter { it.id == itemID }.first())
    }

    fun removeTimerAt(position: Int) {
        this.timers.removeAt(position)
        sortTimers()
    }

    private fun sortTimers() {
        this.timers.sortBy { it.name }
    }


}
