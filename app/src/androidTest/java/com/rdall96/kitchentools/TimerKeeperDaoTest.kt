package com.rdall96.kitchentools

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.persistence.room.Room
import android.content.Context
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import android.util.Log
import com.rdall96.kitchentools.timer.TimeKeeper
import com.rdall96.kitchentools.timer.TimeKeeperDao
import org.junit.*
import org.junit.rules.TestRule
import org.junit.runner.RunWith

/*
 * TimerKeeperDaoTest.kt
 * Project: Kitchen-Tools
 *
 * Author: Ricky Dall'Armellina
 * 3/27/19
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 * 
 */

@RunWith(AndroidJUnit4::class)
class TimeKeeperDaoTest {

    @Rule
    @JvmField
    val rule: TestRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var timeKeeperDao: TimeKeeperDao

    @Before
    fun setup() {
        val context: Context = InstrumentationRegistry.getTargetContext()
        try {
            database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java)
                .allowMainThreadQueries().build()
        } catch (e: Exception) {
            Log.i("test", e.message)
        }
        timeKeeperDao = database.timeKeeperDao()
    }

    @Test
    fun testAddingAndRetrievingData() {
        // 1
        val preInsertRetrievedCategories = timeKeeperDao.getAll()

        // 2
        val timer = TimeKeeper(null, "Example #1", 5, false)
        timeKeeperDao.store(timer)

        //3
        val postInsertRetrievedCategories = timeKeeperDao.getAll()
//        println(postInsertRetrievedCategories)
        val sizeDifference = postInsertRetrievedCategories.size - preInsertRetrievedCategories.size
        Assert.assertEquals(1, sizeDifference)
        val retrievedCategory = postInsertRetrievedCategories.last()
        Assert.assertEquals("Example #1", retrievedCategory.name)
    }

    @After
    fun tearDown() {
        database.close()
    }

}