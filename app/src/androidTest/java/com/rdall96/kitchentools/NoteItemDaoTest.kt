package com.rdall96.kitchentools

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.persistence.room.Room
import android.content.Context
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import android.util.Log
import com.rdall96.kitchentools.note.NoteItem
import com.rdall96.kitchentools.note.NoteItemDao
import org.junit.*
import org.junit.rules.TestRule
import org.junit.runner.RunWith

/*
 * NoteItemDaoTest.kt
 * Project: Kitchen-Tools
 *
 * Author: Ricky Dall'Armellina
 * 3/29/19
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 * 
 */

@RunWith(AndroidJUnit4::class)
class NoteItemDaoTest {

    @Rule
    @JvmField
    val rule: TestRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var noteItemDao: NoteItemDao

    @Before
    fun setup() {
        val context: Context = InstrumentationRegistry.getTargetContext()
        try {
            database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java)
                .allowMainThreadQueries().build()
        } catch (e: Exception) {
            Log.i("test", e.message)
        }
        noteItemDao = database.noteItemDao()
    }

    @Test
    fun testAddingAndRetrievingData() {
        // 1
        val preInsertRetrievedCategories = noteItemDao.getAll()

        // 2
        val note = NoteItem(null, "Some Note", "This note contains some useless text")
        noteItemDao.store(note)

        //3
        val postInsertRetrievedCategories = noteItemDao.getAll()
        val sizeDifference = postInsertRetrievedCategories.size - preInsertRetrievedCategories.size
        Assert.assertEquals(1, sizeDifference)
        val retrievedCategory = postInsertRetrievedCategories.last()
        Assert.assertEquals("Some Note", retrievedCategory.title)
    }

    @After
    fun tearDown() {
        database.close()
    }

}